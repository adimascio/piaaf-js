/* global YASGUI SPARQL_ENDPOINT_URL fetch */

import React from 'react';

import { reprefix, sparql, listQueries, localResourceURI } from '../sparql';

function queryParameters() {
    const params = {}
    if (document.location.search) {
        // .slice(1) to remove the leading '?'
        for (const qp of document.location.search.slice(1).split('&')) {
            const parts = qp.split('=');
            const param = parts.shift();
            if (parts.length) {
                params[param] = decodeURIComponent(parts.join('='));
            }
        }
    }
    return params;
}

function setupYasqeCompletion() {
    const YASQE = YASGUI.YASQE,
        yasqeSettings = YASGUI.defaults.yasqe;

    yasqeSettings.sparql.showQueryButton = true;
    yasqeSettings.sparql.endpoint = SPARQL_ENDPOINT_URL;
    function customPropertyCompleter(yasqe) {
        const returnObj = {
            isValidCompletionPosition: () =>
                YASQE.Autocompleters.properties.isValidCompletionPosition(yasqe),
            preProcessToken: token => YASQE.Autocompleters.properties.preProcessToken(yasqe, token),
            postProcessToken: (token, suggestedString) =>
                YASQE.Autocompleters.properties.postProcessToken(yasqe, token, suggestedString),
        };

        // In this case we assume the properties will fit in memory. So, turn on bulk loading,
        // which will make autocompleting a lot faster
        returnObj.bulk = true;
        returnObj.async = true;
        //and, as everything is in memory, enable autoShowing the completions
        returnObj.autoShow = true;
        returnObj.persistent = 'customProperties'; //this will store the sparql results in the client-cache for a month.
        returnObj.get = (token, callback) => {
            const sparqlQuery = `
SELECT DISTINCT ?p WHERE {?s ?p ?o

FILTER REGEX(?p, "^http://(www.ica.org/standards/RiC/ontology|piaaf.demo.logilab.fr)")
}`;
            sparql(sparqlQuery)
                .then(data => data.map(x => x.p.value))
                .then(callback);
        };
        return returnObj;
    }
    //now register our new autocompleter
    YASQE.registerAutocompleter('customPropertyCompleter', customPropertyCompleter);

    function customClassCompleter(yasqe) {
        const returnObj = {
            isValidCompletionPosition: () =>
                YASQE.Autocompleters.classes.isValidCompletionPosition(yasqe),
            preProcessToken: token => YASQE.Autocompleters.classes.preProcessToken(yasqe, token),
            postProcessToken: (token, suggestedString) =>
                YASQE.Autocompleters.classes.postProcessToken(yasqe, token, suggestedString),
        };
        returnObj.bulk = true;
        returnObj.async = true;
        returnObj.autoShow = true;
        returnObj.get = (token, callback) => {
            const sparqlQuery = `
SELECT DISTINCT ?o WHERE {?s ?p ?o

FILTER REGEX(?o, "^http://(www.ica.org/standards/RiC/ontology|piaaf.demo.logilab.fr)")
}`;
            sparql(sparqlQuery)
                .then(data => data.map(x => x.o.value))
                .then(callback);
        };
        return returnObj;
    }

    YASQE.registerAutocompleter('customClassCompleter', customClassCompleter);
    yasqeSettings.autocompleters = ['customClassCompleter', 'customPropertyCompleter'];
}

// And, to make sure we don't use the other property and class autocompleters,
// overwrite the default enabled completers

class Example extends React.Component {
    constructor(props) {
        super(props);
        this.state = { examples: null };
    }
    componentDidMount() {
        listQueries('Query').then(examples => {
            this.setState({ examples });
        });
    }

    render() {
        const examples = this.state.examples ? this.state.examples : [];
        return (
            <div>
                <span className="examplesTitle">Exemples :</span>
                {Array.prototype.map.call(examples, example => (
                    <span
                        className="example btn btn-default"
                        key={example.title}
                        title={`cliquez pour remplacer la requête
                                actuelle par l'exemple suivant :\n\n${example.sparql}`}
                        data-sparql={example.sparql}
                        onClick={this.props.exampleChanged}>
                        {example.title}
                    </span>
                ))}
            </div>
        );
    }
}

export class SparqlEditor extends React.Component {
    constructor(props) {
        super(props);
        this.exampleChanged = this.exampleChanged.bind(this);
    }

    componentDidMount() {
        YASGUI.YASR.defaults.outputPlugins = ['table', 'error', 'boolean', 'rawResponse'];
        YASGUI.YASR.plugins.table.defaults.getCellContent = this.getCellContent;
        YASGUI.defaults.catalogueEndpoints = [];
        // avoid YASGUI messing up localStorage XXX should be done using YASGUI API
        localStorage.clear();
        YASGUI.defaults.yasqe.sparql.endpoint = SPARQL_ENDPOINT_URL;
        const defaultQuery = `\
        PREFIX ric: <http://www.ica.org/standards/RiC/ontology#>

        SELECT ?entity WHERE {
            ?entity a ric:CorporateBody.
        } LIMIT 10
        `;
        YASGUI.defaults.yasqe.value = queryParameters().q || defaultQuery;
        setupYasqeCompletion();
        this.gui = YASGUI(this.editorDiv, YASGUI.defaults);
    }

    render() {
        return (
            <div>
                <Example exampleChanged={this.exampleChanged} />
                <div
                    id="editor"
                    ref={div => {
                        this.editorDiv = div;
                    }} />
            </div>
        );
    }

    exampleChanged(event) {
        const sparql = event.target.getAttribute('data-sparql');
        this.gui.current().yasqe.setValue(sparql);
    }

    getCellContent(yasr, plugin, bindings, sparqlVar) {
        const binding = bindings[sparqlVar];
        if (binding.type === 'uri') {
            const uri = binding.value;
            // see https://gist.github.com/LaurensRietveld/7d8beca5dbeb6b909b6f
            // to enhance this by combining prefixes with those defined in the query
            return `<a class='uri' href=${localResourceURI(uri)}>${reprefix(uri)}</a>`;
        } else {
            return "<span class='nonUri'>" + formatLiteral(yasr, plugin, binding) + '</span>';
        }
    }
}

// XXX copied from YASR/src/table.js with only eslint adjustments, dunno how to import it
function formatLiteral(yasr, plugin, literalBinding) {
    let stringRepresentation = escapeHtmlEntities(literalBinding.value);
    if (literalBinding['xml:lang']) {
        stringRepresentation =
            '"' + stringRepresentation + '"<sup>@' + literalBinding['xml:lang'] + '</sup>';
    } else if (literalBinding.datatype) {
        const xmlSchemaNs = 'http://www.w3.org/2001/XMLSchema#';
        let dataType = literalBinding.datatype;
        if (dataType.indexOf(xmlSchemaNs) === 0) {
            dataType = 'xsd:' + dataType.substring(xmlSchemaNs.length);
        } else {
            dataType = '&lt;' + dataType + '&gt;';
        }

        stringRepresentation = '"' + stringRepresentation + '"<sup>^^' + dataType + '</sup>';
    }
    return stringRepresentation;
}

// XXX copied from YASR/src/utils.js with only eslint adjustments, dunno how to import it
function escapeHtmlEntities(unescaped) {
    //taken from
    //http://stackoverflow.com/questions/5499078/fastest-method-to-escape-html-tags-as-html-entities
    return unescaped
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}

function etypeQuery(etype, graphname) {
    return `
PREFIX ric: <http://www.ica.org/standards/RiC/ontology#>

SELECT * WHERE {
    GRAPH <${graphname}> {
        ?entity a ric:${etype}
    }
}
    `;
}

export function sparqlUrl(etype, graphname) {
    const query = etypeQuery(etype, graphname);
    return `/sparql?q=${encodeURIComponent(query)}`;
}
