// hardcoded, don't want to query LOV just for that right now.
export default {
    'owl:sameAs': {
        comment: {
            fr: null,
            en: 'The property that determines that two given individuals are equal.',
        },
        label: {
            fr: 'identique à',
            en: 'same as',
        },
    },
    'rdfs:label': {
        label: {
            fr: 'Intitulé',
            en: 'Label',
        },
        comment: {
            fr: null,
            en: null,
        },
    },
    'rdf:type': {
        label: {
            fr: 'type de la ressource',
            en: 'resource type',
        },
        comment: {
            fr: null,
            en: null,
        },
    },
    'rdfs:seeAlso': {
        label: {
            fr: 'voir aussi',
            en: 'see also',
        },
        comment: {
            fr: null,
            en: null,
        },
    },
    'isni:identifierValid': {
        label: {
            fr: 'Identifiant ISNI',
            en: 'ISNI Identifier',
        },
        comment: {
            fr: null,
            en: null,
        },
    },
    'skos:prefLabel': {
        label: {
            fr: 'dénomination usuelle',
            en: 'preferred label',
        },
        comment: {
            fr: 'La dénomination préférée pour une ressource, dans un langage donné',
            en: 'The preferred lexical label for a resource, in a given language',
        },
    },
    'skos:altLabel': {
        label: {
            fr: 'dénomination alternative',
            en: 'alternate label',
        },
        comment: {
            fr: 'Une dénomination alternative pour une ressource',
            en: 'An alternative lexical label for a resource',
        },
    },
    'ric:noteOnProcessAppplied': {
        label: {
            fr: 'note sur les traitements réalisés',
            en: 'note on applied processes',
        },
        comment: {
        },
    },
    'ric:associatedWithRelation': {
        'label': {
            'fr': 'en relation avec',
            'en': 'associated with',
        },
    },
    'ric:thingPrecedes': {
        'label': {
            'fr': 'pr\u00e9c\u00e8de',
            'en': 'thing precedes',
        },
    },
    'ric:Agent': {
        'label': {
            'fr': 'Agent',
            'en': 'Agent',
        },
    },
    'ric:mainSubject': {
        'label': {
            'fr': 'a pour principal sujet',
            'en': 'main subject',
        },
    },
    'ric:affects': {
        'label': {
            'fr': 'a affect\u00e9',
            'en': 'affected',
        },
    },
    'ric:followingThingInTime': {
        'label': {
            'fr': 'entit\u00e9 suivante dans le temps',
            'en': 'following thing in time',
        },
    },
    'ric:categorizationOf': {
        'label': {
            'fr': 'a pour cat\u00e9gorie',
            'en': 'has type',
        },
    },
    'ric:arrangement': {
        'label': {
            'fr': 'principes de classement',
            'en': 'arrangement',
        },
    },
    'ric:hasLocation': {
        'label': {
            'fr': 'localisation \u00e0',
            'en': 'has location',
        },
    },
    'ric:memberOf': {
        'label': {
            'fr': 'est inclus dans',
            'en': 'member of',
        },
    },
    'ric:hasAgentMember': {
        'label': {
            'fr': 'a pour membre',
            'en': 'has agent member',
        },
    },
    'ric:AgentMembershipRelation': {
        'label': {
            'fr': 'Relation d\u2019appartenance (entre un agent et un groupe)',
            'en': 'Agent Membership Relation',
        },
    },
    'ric:Event': {
        'label': {
            'fr': '\u00c9v\u00e9nement',
            'en': 'Event',
        },
    },
    'ric:partOf': {
        'label': {
            'fr': 'ressource constituant le tout',
            'en': 'part of',
        },
    },
    'ric:AgentName': {
        'label': {
            'fr': 'Nom d\u2019agent',
            'en': 'Agent Name',
        },
    },
    'ric:TemporalRelation': {
        'label': {
            'fr': 'Relation chronologique',
            'en': 'Temporal Relation',
        },
    },
    'ric:position': {
        'label': {
            'fr': 'poste existant au sein de cette entit\u00e9',
            'en': 'has position',
        },
    },
    'ric:describedBy': {
        'label': {
            'fr': 'est d\u00e9crit(e) par',
            'en': 'described by',
        },
    },
    'ric:beginningDate': {
        'label': {
            'fr': 'date de d\u00e9but',
            'en': 'beginning date',
        },
    },
    'ric:authorityHeldBy': {
        'label': {
            'fr': 'autorit\u00e9 d\u00e9tenue par',
            'en': 'authority held by',
        },
    },
    'ric:groupHasMember': {
        'label': {
            'fr': 'a pour membre',
            'en': 'group has member',
        },
    },
    'ric:hasLocationRelation': {
        'label': {
            'fr': 'a pour localisation',
            'en': 'has location relation',
        },
    },
    'ric:Mandate': {
        'label': {
            'fr': 'R\u00e8gle de r\u00e9f\u00e9rence',
            'en': 'Mandate',
        },
    },
    'ric:hasTitle': {
        label: {
            fr: 'Intitulé RiC',
            en: 'RiC Label',
        },
    },
    'ric:hasType': {
        'label': {
            'fr': 'cat\u00e9gorie ou type',
            'en': 'has type',
        },
    },
    'ric:evidencedBy': {
        'label': {
            'fr': 'a pour source',
            'en': 'evidenced by',
        },
    },
    'ric:AgentControlRelation': {
        'label': {
            'fr': 'Relation de contr\u00f4le entre agents',
            'en': 'Agent Control Relation',
        },
    },
    'ric:endDate': {
        'label': {
            'fr': 'date de fin',
            'en': 'end date',
        },
    },
    'ric:hasBusinessRelation': {
        'label': {
            'fr': 'en relation fonctionnelle avec',
            'en': 'has business relation with',
        },
    },
    'ric:BusinessRelation': {
        'label': {
            'fr': 'Relation fonctionnelle',
            'en': 'Business Relation',
        },
    },
    'ric:SocialRelation': {
        'label': {
            'fr': 'Relation sociale',
            'en': 'Social Relation',
        },
    },
    'ric:RecordSet': {
        'label': {
            'fr': 'Groupe de documents',
            'en': 'Record Set',
        },
    },
    'ric:noteOnRelatedMaterial': {
        'label': {
            'fr': 'note sur les documents en relation',
            'en': 'note on related material',
        },
    },
    'ric:denominatedBy': {
        'label': {
            'fr': 'donne comme nom',
            'en': 'denominated by',
        },
    },
    'ric:agentFollows': {
        'label': {
            'fr': 'successeur de',
            'en': 'agent follows',
        },
    },
    'ric:agentIsInferiorInHierarchicalRelation': {
        'label': {
            'fr': 'a pour entit\u00e9 hi\u00e9rarchiquement sup\u00e9rieure',
            'en': 'agent is inferior in hierarchical relation',
        },
    },
    'ric:isRelatedWith': {
        'label': {
            'fr': 'a une relation avec la ressource',
            'en': 'is related with',
        },
    },
    'ric:date': {
        'label': {
            'fr': 'date',
            'en': 'date',
        },
    },
    'ric:FunctionAbstract': {
        'label': {
            'fr': "Domaine d'activité",
            'en': 'Activity domain',
        },
    },
    'ric:identifiedBy': {
        'label': {
            'fr': 'identifiant',
            'en': 'identified by',
        },
    },
    'ric:recordSetType': {
        'label': {
            'fr': 'cat\u00e9gorie de ce groupe de documents',
            'en': 'record set type',
        },
    },
    'ric:controlOnAgent': {
        'label': {
            'fr': 'contr\u00f4le ou tutelle exerc\u00e9 sur',
            'en': 'control on agent',
        },
    },
    'ric:agentHasLeadershipRelation': {
        'label': {
            'fr': 'dirige',
            'en': 'entit\u00e9 dirig\u00e9e',
        },
    },
    'ric:mainSubjectOf': {
        'label': {
            'fr': 'est le principal sujet de',
            'en': 'main subject of',
        },
    },
    'ric:FindingAid': {
        'label': {
            'fr': 'Instrument de recherche',
            'en': 'Finding Aid',
        },
    },
    'ric:leadBy': {
        'label': {
            'fr': 'dirig\u00e9 par',
            'en': 'lead by',
        },
    },
    'ric:locationOf': {
        'label': {
            'fr': 'localisation de',
            'en': 'location of',
        },
    },
    'ric:hasAuthorityRelation': {
        'label': {
            'fr': 'a autorit\u00e9 sur',
            'en': 'has authority relation',
        },
    },
    'ric:GroupType': {
        'label': {
            'fr': 'Cat\u00e9gorie de collectivit\u00e9',
            'en': 'Corporate Body Type',
        },
    },
    'ric:ArchivalProvenanceRelation': {
        'label': {
            'fr': 'Relation de provenance archivistique',
            'en': 'Archival Provenance Relation',
        },
    },
    'ric:underAuthorityRelation': {
        'label': {
            'fr': 'est sous l\u2019autorit\u00e9 de',
            'en': 'under authority relation',
        },
    },
    'ric:subjectOf': {
        'label': {
            'fr': 'est le sujet de',
            'en': 'subject of',
        },
    },
    'ric:AgentWholePartRelation': {
        'label': {
            'fr': 'Relation partitive entre agents',
            'en': 'Agent Whole Part Relation',
        },
    },
    'ric:conformsToRule': {
        'label': {
            'fr': 'a pour r\u00e8gle de r\u00e9f\u00e9rence',
            'en': 'conforms to rule',
        },
    },
    'ric:created': {
        'label': {
            'fr': 'a produit',
            'en': 'created',
        },
    },
    'ric:lastUpdateDate': {
        'label': {
            'fr': 'date de la derni\u00e8re mise \u00e0 jour',
            'en': 'last update date',
        },
    },
    'ric:certainty': {
        'label': {
            'fr': 'certitude',
            'en': 'certainty',
        },
    },
    'ric:authorityOn': {
        'label': {
            'fr': 'autorit\u00e9 sur',
            'en': 'authority on',
        },
    },
    'ric:noteOnProvenance': {
        'label': {
            'fr': 'note sur la provenance',
            'en': 'note on provenance',
        },
    },
    'ric:history': {
        'label': {
            'fr': 'histoire',
            'en': 'history',
        },
    },
    'ric:hasSocialRelation': {
        'label': {
            'fr': 'en relation sociale avec',
            'en': 'has social relation',
        },
    },
    'ric:scopeAndContent': {
        'label': {
            'fr': 'pr\u00e9sentation du contenu',
            'en': 'scope and content',
        },
    },
    'ric:hasPart': {
        'label': {
            'fr': 'a pour partie',
            'en': 'has part',
        },
    },
    'ric:relationAssociates': {
        'label': {
            'fr': 'relie',
            'en': 'associates',
        },
    },
    'ric:hasAgentControlRelation': {
        'label': {
            'fr': 'contr\u00f4le ou a la tutelle de',
            'en': 'agent controls',
        },
    },
    'ric:AuthorityRelation': {
        'label': {
            'fr': 'Relation d\u2019autorit\u00e9',
            'en': 'Authority Relation',
        },
    },
    'ric:WholePartRelation': {
        'label': {
            'fr': 'Relation partitive',
            'en': 'Whole Part Relation',
        },
    },
    'ric:contentType': {
        'label': {
            'fr': 'type de contenu',
            'en': 'content type',
        },
    },
    'piaaf:isFunctionTypeOf': {
        'label': {
            'fr': 'est le domaine d\u2019activit\u00e9 de',
            'en': 'is function type of',
        },
    },
    'ric:placeType': {
        'label': {
            'fr': 'type de lieu',
            'en': 'place type',
        },
    },
    'ric:proxyIn': {
        'label': {
            'fr': 'est le contexte local de',
            'en': 'proxy in',
        },
    },
    'ric:creationDate': {
        'label': {
            'fr': 'date de cr\u00e9ation',
            'en': 'creation date',
        },
    },
    'ric:FamilyRelation': {
        'label': {
            'fr': 'Relation familiale',
            'en': 'Family Relation',
        },
    },
    'ric:occupies': {
        'label': {
            'fr': 'occupe le poste de',
            'en': 'occupies',
        },
    },
    'ric:createdBy': {
        'label': {
            'fr': 'a \u00e9t\u00e9 produit par',
            'en': 'created by',
        },
    },
    'ric:groupTypeOf': {
        'label': {
            'fr': 'est la cat\u00e9gorie de collectivit\u00e9 de',
            'en': 'group type of',
        },
    },
    'ric:jurisdiction': {
        'label': {
            'fr': 'ressort',
            'en': 'jurisdiction',
        },
    },
    'ric:TypeRelation': {
        'label': {
            'fr': 'Relation de cat\u00e9gorisation',
            'en': 'Type Relation',
        },
    },
    'ric:noteOnAcquisition': {
        'label': {
            'fr': 'note sur les modalit\u00e9s d\u2019entr\u00e9e',
            'en': 'note on acquisition',
        },
    },
    'ric:leadershipWithPosition': {
        'label': {
            'fr': 'au poste de',
            'en': 'leadership with position',
        },
    },
    'ric:agentHasMembershipRelation': {
        'label': {
            'fr': 'membre de',
            'en': 'agent has membership relation',
        },
    },
    'ric:agentHasWholePartRelation': {
        'label': {
            'fr': 'subdivision de',
            'en': 'agent has whole-part relation',
        },
    },
    'ric:Record': {
        'label': {
            'fr': 'Document',
            'en': 'Record',
        },
    },
    'ric:publishedBy': {
        'label': {
            'fr': '\u00e9dit\u00e9 par',
            'en': 'published by',
        },
    },
    'ric:deathDate': {
        'label': {
            'fr': 'date de d\u00e9c\u00e8s',
            'en': 'death date',
        },
    },
    'ric:provenanceEntity': {
        'label': {
            'fr': 'provenance archivistique',
            'en': 'provenance entity',
        },
    },
    'ric:isTypeOf': {
        'label': {
            'fr': 'est la cat\u00e9gorie ou le type de',
            'en': 'categorizes',
        },
    },
    'ric:followingAgent': {
        'label': {
            'fr': 'a pour successeur',
            'en': 'following agent',
        },
    },
    'ric:physicalOrLogicalExtent': {
        'label': {
            'fr': 'nombre d\u2019unit\u00e9s physiques ou logiques',
            'en': 'physical or logical extent',
        },
    },
    'ric:hasHierarchicalChild': {
        'label': {
            'fr': 'a pour agent subordonn\u00e9',
            'en': 'has hierarchical child',
        },
    },
    'ric:holderOf': {
        'label': {
            'fr': 'documents conserv\u00e9s par cette institution',
            'en': 'holder of',
        },
    },
    'ric:describes': {
        'label': {
            'fr': 'd\u00e9crit',
            'en': 'describes',
        },
    },
    'ric:CorporateBody': {
        'label': {
            'fr': 'Collectivit\u00e9',
            'en': 'Corporate Body',
        },
    },
    'ric:positionIn': {
        'label': {
            'fr': 'poste au sein de l\u2019entit\u00e9',
            'en': 'position in',
        },
    },
    'ric:recordSetTypeOf': {
        'label': {
            'fr': 'est la cat\u00e9gorie du groupe de documents',
            'en': 'record set type of',
        },
    },
    'ric:leadershipOn': {
        'label': {
            'fr': 'direction de',
            'en': 'leadership on',
        },
    },
    'ric:AgentTemporalRelation': {
        'label': {
            'fr': 'Relation chronologique entre agents',
            'en': 'Agent Succession Relation',
        },
    },
    'ric:isDenominationOf': {
        'label': {
            'fr': 'est le nom de',
            'en': 'is denomination of',
        },
    },
    'ric:LocationRelation': {
        'label': {
            'fr': 'Relation de localisation',
            'en': 'Location Relation',
        },
    },
    'ric:hasTypeRelation': {
        'label': {
            'fr': 'a pour cat\u00e9gorie ou type',
            'en': 'has type',
        },
    },
    'ric:member': {
        'label': {
            'fr': 'inclut',
            'en': 'member',
        },
    },
    'ric:AgentLeadershipRelation': {
        'label': {
            'fr': 'Relation de direction (entre une personne et un groupe)',
            'en': 'Agent Leadership relation',
        },
    },
    'ric:description': {
        'label': {
            'fr': 'description',
            'en': 'description',
        },
    },
    'ric:precedingAgent': {
        'label': {
            'fr': 'a pour pr\u00e9d\u00e9cesseur',
            'en': 'preceding agent',
        },
    },
    'ric:heldBy': {
        'label': {
            'fr': 'institution de conservation',
            'en': 'held by',
        },
    },
    'ric:affectedBy': {
        'label': {
            'fr': 'a \u00e9t\u00e9 affect\u00e9 par',
            'en': 'affected by',
        },
    },
    'ric:agentControlHeldBy': {
        'label': {
            'fr': 'contr\u00f4le ou tutelle exerc\u00e9 par',
            'en': 'agent control held by',
        },
    },
    'ric:agentPrecedes': {
        'label': {
            'fr': 'pr\u00e9d\u00e9cesseur de',
            'en': 'agent precedes',
        },
    },
    'ric:hasNext': {
        'label': {
            'fr': 'est suivi dans la s\u00e9quence par',
            'en': 'has next',
        },
    },
    'ric:publicationDate': {
        'label': {
            'fr': 'date de publication',
            'en': 'publication date',
        },
    },
    'ric:birthDate': {
        'label': {
            'fr': 'date de naissance',
            'en': 'birth date',
        },
    },
    'ric:hasDenominationRelation': {
        'label': {
            'fr': 'a pour nom',
            'en': 'has name',
        },
    },
    'piaaf:functionComponentList': {
        'label': {
            'fr': 'liste de composants de ce domaine d\u2019activit\u00e9',
            'en': 'function component list',
        },
    },
    'ric:subject': {
        'label': {
            'fr': 'a pour sujet',
            'en': 'subject',
        },
    },
    'ric:groupHasPart': {
        'label': {
            'fr': 'a pour subdivision',
            'en': 'group has part',
        },
    },
    'ric:originatedObject': {
        'label': {
            'fr': 'ressource produite',
            'en': 'originated object',
        },
    },
    'ric:isAuthorOf': {
        'label': {
            'fr': 'est l\u2019auteur de',
            'en': 'is author of',
        },
    },
    'ric:conditionsOfAccess': {
        'label': {
            'fr': 'conditions d\u2019acc\u00e8s',
            'en': 'conditions of access',
        },
    },
    'ric:hasHierarchicalParent': {
        'label': {
            'fr': 'a pour agent sup\u00e9rieur',
            'en': 'has hierarchical parent',
        },
    },
    'piaaf:latitude': {
        'label': {
            'fr': 'latitude',
            'en': 'latitude',
        },
    },
    'ric:isLocationOf': {
        'label': {
            'fr': 'est la localisation de',
            'en': 'is location of',
        },
    },
    'ric:hasWholePartRelation': {
        'label': {
            'fr': 'est une partie de',
            'en': 'has whole-part relation',
        },
    },
    'ric:AgentHierarchicalRelation': {
        'label': {
            'fr': 'Relation hi\u00e9rarchique entre agents',
            'en': 'Agent Hierarchical Relation',
        },
    },
    'ric:originatedBy': {
        'label': {
            'fr': 'a pour provenance archivistique',
            'en': 'originated by',
        },
    },
    'ric:accruals': {
        'label': {
            'fr': 'accroissements possibles',
            'en': 'accruals',
        },
    },
    'ric:digitalCopy': {
        'label': {
            'fr': 'a pour copie num\u00e9rique',
            'en': 'has digital copy',
        },
    },
    'ric:Description': {
        'label': {
            'fr': 'Notice',
            'en': 'Description',
        },
    },
    'ric:familyRelationWith': {
        'label': {
            'fr': 'relation familiale avec',
            'en': 'family relation with',
        },
    },
    'ric:isDigitalCopyOf': {
        'label': {
            'fr': 'est la copie num\u00e9rique de',
            'en': 'is digital copy of',
        },
    },
    'ric:denominationOf': {
        'label': {
            'fr': 'nom assign\u00e9 \u00e0',
            'en': 'denomination of',
        },
    },
    'ric:DenominationRelation': {
        'label': {
            'fr': 'Relation de d\u00e9nomination',
            'en': 'Denomination Relation',
        },
    },
    'ric:wholeHasPart': {
        'label': {
            'fr': 'partie de la ressource',
            'en': 'whole has part',
        },
    },
    'ric:businessRelationWith': {
        'label': {
            'fr': 'relation fonctionnelle avec',
            'en': 'business relation with',
        },
    },
    'ric:published': {
        'label': {
            'fr': '\u00e9diteur de',
            'en': 'published',
        },
    },
    'ric:proxyFor': {
        'label': {
            'fr': 'est repr\u00e9sent\u00e9 par (dans le contexte local)',
            'en': 'proxy for',
        },
    },
    'ric:RecordSetType': {
        'label': {
            'fr': 'Cat\u00e9gorie de groupe de documents',
            'en': 'Record Set Type',
        },
    },
    'ric:Proxy': {
        'label': {
            'fr': 'Repr\u00e9sentant (d\u2019un document ou d\u2019un groupe de documents)',
            'en': 'Proxy',
        },
    },
    'ric:thingFollows': {
        'label': {
            'fr': 'succ\u00e8de \u00e0',
            'en': 'thing follows',
        },
    },
    'ric:agentControlledBy': {
        'label': {
            'fr': 'sous le contr\u00f4le ou la tutelle de',
            'en': 'agent under control of',
        },
    },
    'ric:agentPartOf': {
        'label': {
            'fr': 'subdivision de',
            'en': 'agent part of',
        },
    },
    'ric:TopRecordSet': {
        'label': {
            'fr': 'Groupe de documents au plus haut niveau',
            'en': 'Top Record Set',
        },
    },
    'ric:agentMembershipIn': {
        'label': {
            'fr': 'appartenance \u00e0',
            'en': 'agent membership in',
        },
    },
    'ric:hasFamilyRelation': {
        'label': {
            'fr': 'en relation familiale avec',
            'en': 'has family relation',
        },
    },
    'ric:groupType': {
        'label': {
            'fr': 'appartient \u00e0 la cat\u00e9gorie de collectivit\u00e9',
            'en': 'group type',
        },
    },
    'ric:noteOnStructureOrGenealogy': {
        'label': {
            'fr': 'note sur l\u2019organisation interne',
            'en': 'note on structure or genealogy',
        },
    },
    'ric:wholeHasAgentPart': {
        'label': {
            'fr': 'subdivision de l\u2019agent',
            'en': 'whole has agent part',
        },
    },
    'ric:leadershipBy': {
        'label': {
            'fr': 'a pour dirigeant',
            'en': 'leadership by',
        },
    },
    'ric:Position': {
        'label': {
            'fr': 'Poste',
            'en': 'Position',
        },
    },
    'ric:isEvidenceOf': {
        'label': {
            'fr': 'est la source de',
            'en': 'is evidence of',
        },
    },
    'ric:agentIsSuperiorInHierarchicalRelation': {
        'label': {
            'fr': 'a pour entit\u00e9 hi\u00e9rarchiquement inf\u00e9rieure',
            'en': 'agent is superior in hierarchical relation',
        },
    },
    'ric:hasProvenanceRelation': {
        'label': {
            'fr': 'est la provenance archivistique de',
            'en': 'has archival provenance relation',
        },
    },
    'ric:precedingThingInTime': {
        'label': {
            'fr': 'entit\u00e9 pr\u00e9c\u00e9dente dans le temps',
            'en': 'preceding thing in time',
        },
    },
    'ric:occupiedBy': {
        'label': {
            'fr': 'poste occup\u00e9 par',
            'en': 'occupied by',
        },
    },
    'ric:authoredBy': {
        'label': {
            'fr': 'a pour auteur',
            'en': 'authored by',
        },
    },
    'ric:Place': {
        'label': {
            'fr': 'Lieu',
            'en': 'Place',
        },
    },
    'piaaf:longitude': {
        'label': {
            'fr': 'longitude',
            'en': 'longitude',
        },
    },
    'ric:Person': {
        'label': {
            'fr': 'Personne',
            'en': 'Person',
        },
    },
    'piaaf:hasFunctionOfType': {
        'label': {
            'fr': 'a pour domaine d\u2019activit\u00e9',
            'en': 'has function of type',
        },
    },
};
