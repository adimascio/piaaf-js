import React from 'react';
import { NavLink } from 'react-router-dom';

import { Loading } from './helpers';
import { sparql } from '../sparql';

/**
 * fetch component's pageid either directly from its props or from its router context.
 * @param {*} props Page props
 */
function pageid(props) {
    return props.pageid === undefined ? props.match.params.pageid : props.pageid;
}

const editorialPages = [
    ['/editorial/historique', 'Enjeux, objectifs et historique du projet'],
    ['/editorial/contexte-technique', 'Réalisation'],
    ['/stats/', 'Contenu du portail'],
    ['/editorial/en-savoir-plus', 'En savoir plus'],
    ['/editorial/contact', 'Nous contacter'],
];

class Page extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: null,
            content: null,
        };
    }

    componentWillReceiveProps(nextProps) {
        const currentPageid = pageid(this.props),
            nextPageid = pageid(nextProps);
        if (nextPageid !== currentPageid) {
            this.fetchData(nextPageid);
        }
    }

    componentDidMount() {
        this.fetchData(pageid(this.props));
    }

    fetchData(pageId) {
        sparql(
            `
PREFIX piaaf: <http://piaaf.demo.logilab.fr/ontology#>

SELECT ?title ?content
WHERE {
     BIND (<http://piaaf.demo.logilab.fr/ui/${pageId}> as ?page)
     ?page piaaf:content ?content;
           rdfs:label ?title.
}
`,
            { credentials: 'include' },
        ).then(bindings => {
            if (bindings.length) {
                this.setState({
                    title: bindings[0].title.value,
                    content: bindings[0].content.value,
                });
            } else {
                this.setState({
                    title: pageId,
                    content: 'Contenu à venir.',
                });
            }
        });
    }

    render() {
        if (this.state.title === null) {
            return <Loading />;
        }
        return (
            <div>
                <h1>{this.state.title}</h1>
                <div dangerouslySetInnerHTML={{ __html: this.state.content }} />
            </div>
        );
    }
}

export function PageWithNavigation(props) {
    return (
        <div className="row">
            <div className="col-sm-2">
                <Sidebar linkdefs={editorialPages} />
            </div>
            <div className="col-sm-9">
                <Page {...props} />
            </div>
        </div>
    );
}

function Sidebar({ linkdefs }) {
    return (
        <ul id="sidebar" className="nav nav-stacked">
            {linkdefs.map(([path, title]) => (
                <li key={path}>
                    <NavLink to={path} exact>
                        {title}
                    </NavLink>
                </li>
            ))}
        </ul>
    );
}
