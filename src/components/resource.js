import React from 'react';
import { pick, last } from 'lodash';

import { NavLink } from 'react-router-dom';

import { NotFound } from '.';
import { Loading } from './helpers';

import {
    describe,
    sparql,
    sparqlGraphQuery,
    parseYear,
    localResourceURI,
    propLabel,
    propComment,
    fetchSameAsOtherGraphs,
    GRAPH_NAMES,
    fetchPrevNext,
    reprefix,
} from '../sparql';
import { MAX_YEAR, drawHierarchicalTimelineSVG } from '../htl';
import { SigmaRDFGraph } from './graph';

export function AttributePropertyView(props) {
    const property = props.property,
        values = props.values;
    values.sort();
    return (
        <div>
            <span title={propComment(property, property)} className="property">
                {propLabel(property)}
            </span>
            <div className="values">
                {Array.prototype.map.call(values, value => (
                    <span className="value literal" key={value}>
                        {value}
                    </span>
                ))}
            </div>
        </div>
    );
}

export function RelationPropertyView(props) {
    const property = props.property,
        values = props.values;
    values.sort();
    return (
        <div>
            <span title={propComment(property, property)} className="property">
                {propLabel(property)}
            </span>
            <div className="values">
                {Array.prototype.map.call(values, ([label, value]) => (
                    <span className="value uri" key={value}>
                        <NavLink to={`${localResourceURI(value)}`}>{label}</NavLink>
                    </span>
                ))}
            </div>
        </div>
    );
}

function StartStop({ start, stop }) {
    if (start && stop) {
        return (
            <span style={{ marginRight: '0.5em' }}>
                [{start} - {stop}]
            </span>
        );
    } else if (start) {
        return <span style={{ marginRight: '0.5em' }}>[{start}]</span>;
    } else {
        return null;
    }
}

function MoreInfos({ link }) {
    return (
        <span style={{ marginLeft: '0.5em', fontSize: '80%' }}>
            <a href={localResourceURI(link)}>Plus d'infos</a>
        </span>
    );
}

export function PackedRelationPropertyView(props) {
    const property = props.property,
        values = props.values;
    return (
        <div>
            <span title={propComment(property, property)} className="property">
                {propLabel(property)}
            </span>
            <div className="values">
                {values.map((reldescr, i) => (
                    <div key={`reldescr${i}`} className="packeditem">
                        <StartStop {...reldescr} />
                        <span>
                            <a href={localResourceURI(reldescr.relations[0].desturi)}>
                                {reldescr.relations[0].destlabel}
                            </a>
                        </span>
                        <MoreInfos link={reldescr.relationuri} />
                    </div>
                ))}
            </div>
        </div>
    );
}

export function AttributePropertiesView(props) {
    const properties = Object.keys(props.relations);
    properties.sort();
    return (
        <ul className="list-group">
            {Array.prototype.map.call(properties, property => (
                <li className="list-group-item" key={property}>
                    <AttributePropertyView property={property} values={props.relations[property]} />
                </li>
            ))}
        </ul>
    );
}

export function RelationPropertiesView(props) {
    const properties = Object.keys(props.relations);
    // sort properties lexically but with type first
    properties.sort(function (p1, p2) {
        if (p1 === 'rdf:type') {
            return -1;
        }
        if (p2 === 'rdf:type') {
            return 1;
        }
        return p1.localeCompare(p2);
    });

    return (
        <ul className="list-group">
            {Array.prototype.map.call(properties, property => (
                <li className="list-group-item" key={property}>
                    <RelationPropertyView property={property} values={props.relations[property]} />
                </li>
            ))}
        </ul>
    );
}

export function PackedRelationPropertiesView(props) {
    const properties = Object.keys(props.relations);
    // sort properties lexically but with type first
    properties.sort(function (p1, p2) {
        if (p1 === 'rdf:type') {
            return -1;
        }
        if (p2 === 'rdf:type') {
            return 1;
        }
        return p1.localeCompare(p2);
    });

    return (
        <ul className="list-group">
            {Array.prototype.map.call(properties, property => (
                <li className="list-group-item" key={property}>
                    <PackedRelationPropertyView
                        property={property}
                        values={props.relations[property]}/>
                </li>
            ))}
        </ul>
    );
}

export function PrevNext(props) {
    return (
        <nav id="prevnext">
            {props.prev && (
                <a className="pull-left" href={props.prev.uri}>
                    &laquo; {props.prev.label}
                </a>
            )}
            {props.next && (
                <a className="pull-right" href={props.next.uri}>
                    {props.next.label} &raquo;
                </a>
            )}
        </nav>
    );
}

export class RDFResource extends React.Component {
    constructor(props) {
        super(props);
        const uri = props.uri || decodeURIComponent(props.match.params.uri);
        this.state = { relations: null, sameAs: {}, uri, prevNext: null };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.match.params.uri !== this.props.match.params.uri) {
            this.setState({ relations: null });
            const uri = decodeURIComponent(nextProps.match.params.uri);
            this.fetchData(uri);
        }
    }

    fetchData(uri) {
        Promise.all([describe(uri), fetchSameAsOtherGraphs(uri), fetchPrevNext(uri)]).then(
            ([relations, sameAs, prevNext]) => {
                this.setState({ relations, uri, sameAs, prevNext });
            },
        );
    }

    componentDidMount() {
        this.fetchData(this.state.uri);
    }

    resourceType() {
        if (
            this.state.relations &&
            this.state.relations['subject-relations']['rdf:type'] &&
            this.state.relations['subject-relations']['rdf:type'].length
        ) {
            const [label, uri] = this.state.relations['subject-relations']['rdf:type'][0];
            return { label, uri, quri: reprefix(uri) };
        }
        return null;
    }

    render() {
        const relations = this.state.relations;
        if (relations === null) {
            return <Loading />;
        } else if (Object.values(relations).every(props => Object.keys(props).length === 0)) {
            // if there are no attributes, subject nor object relations, we may
            // suppose this is an unknown resource and return appropriate
            // content.
            return <NotFound />;
        }
        const resourceType = this.resourceType();
        const viewProps = Object.assign(
            { resourceType, history: this.props.history },
            pick(this.state, 'uri', 'relations', 'prevNext', 'sameAs'),
        );
        return <RDFResourceDefaultView {...viewProps} />;
    }
}

function RDFResourceDefaultView(props) {
    const { relations, resourceType, sameAs, uri, prevNext } = props;
    const title = takeFirst(relations.attributes, 'skos:prefLabel', 'rdfs:label'),
        graphQuery = sparqlGraphQuery(`<${uri}> as ?entity`);
    return (
        <div>
            {prevNext && <PrevNext prev={prevNext.prev} next={prevNext.next} />}
            <div className="clear" />
            <h1>
                {resourceType && <span id="resourceType">[{propLabel(resourceType.quri)}]</span>}
                {title || uri}
                <a
                    className="glyphicon glyphicon-globe resourceExtLink"
                    href={uri}
                    title="Naviguez vers cette ressource"/>
            </h1>
            {title && (
                <div className="resourceUri text-muted">
                    {uri}
                    {Object.keys(GRAPH_NAMES)
                        .filter(gid => gid in sameAs)
                        .map(gid => (
                            <a
                                key={gid}
                                className={'sameas ' + (uri === sameAs[gid].uri ? 'active' : '')}
                                href={sameAs[gid].localuri}
                                title={'Cette ressource décrite chez ' + GRAPH_NAMES[gid]}>
                                @{GRAPH_NAMES[gid]}
                            </a>
                        ))}
                </div>
            )}
            <SigmaRDFGraph query={graphQuery} fromBaseSet={node => node.id === uri} />
            <ControlTimeline uri={uri} attributes={relations.attributes} history={props.history} />
            <div className="resource-data">
                {Object.keys(relations['attributes']).length > 0 && (
                    <div>
                        <h2>Attributs</h2>
                        <AttributePropertiesView relations={relations['attributes']} />
                    </div>
                )}
                {Object.keys(relations['subject-relations']).length > 0 && (
                    <div>
                        <h2>Relations</h2>
                        <RelationPropertiesView relations={relations['subject-relations']} />
                    </div>
                )}
                {Object.keys(relations['packed-relations']).length > 0 && (
                    <div>
                        <PackedRelationPropertiesView relations={relations['packed-relations']} />
                    </div>
                )}
                {Object.keys(relations['object-relations']).length > 0 && (
                    <div>
                        <h2>Relations ayant cette ressource comme objet</h2>
                        <RelationPropertiesView relations={relations['object-relations']} />
                    </div>
                )}
            </div>
        </div>
    );
}

class ControlTimeline extends React.Component {
    constructor(props) {
        super(props);
        this.state = { graph: null };
    }

    fetchData(uri, attributes) {
        temporalHierarchicalGraphData(uri, attributes).then(graph => {
            this.setState({ graph });
            const svgDom = document.querySelector('svg.htl');
            if (graph.nodes.length > 1) {
                drawHierarchicalTimelineSVG('svg.htl', uri, graph, this.props.history);
                svgDom.style.display = 'block';
                svgDom.style.width = graph.width + 'px';
                svgDom.style.height = graph.height + 'px';
            }
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.uri !== this.props.uri) {
            this.setState({ graphData: null });
            this.fetchData(nextProps.uri, nextProps.attributes);
        }
    }

    componentDidMount() {
        this.fetchData(this.props.uri, this.props.attributes);
    }

    render() {
        return <svg className="htl" />;
    }
}

// Given a resource URI, return a promise on a structure describing:
// * resource's chronological information (preceding and following agents)
// * resource's hierarchical information (parent/controller and child/controllee
//   agents)
//
// This information is returned as a "graph" definition {nodes,
// chronologicalLinks, hierarchicalLinks}.
//
// nodes is a list of nodes with the following properties:
// - uri,
// - label,
// - begin (year as number),
// - end (year as number),
// - precedingLinks (references to chronological links with this node as
//   target),
// - followingLinks (references to chronological links with this node as
//   source),
// - hierarchicalChildLinks (references to hierarchical links with this node as
//   parent),
// - hierarchicalParentLinks (references to hierarchical links with this node as
//   child),
//
// chronologicalLinks is list of links with the following properties :
// - preceding (reference to the predecessor node in the relation),
// - following (reference to the successor node in the relation),
// - begin (year as number),
// - end (year as number),
//
// hierarchicalLinks is list of links with the following properties :
// - hierarchicalParent (reference to the parent node in the relation),
// - hierarchicalchild (reference to the child node in the relation),
// - begin (year as number),
// - end (year as number),
//
// Nodes, chronological and hierarchical links without begin date are skipped.

function temporalHierarchicalGraphData(uri, attributes) {
    // complete starting node by setting its label, begin and end dates from
    // given attributes (those are not fetched since only nodes starting
    // from it are fetched)
    const nodesByUri = {
            [uri]: buildNode(
                uri,
                takeFirst(attributes, 'skos:prefLabel', 'rdfs:label'),
                takeFirstInt(attributes, 'ric:birthDate', 'ric:beginningDate'),
                takeFirstInt(attributes, 'ric:deathDate', 'ric:endDate'),
            ),
        },
        links = {
            hierarchical: [],
            chronological: [],
        },
        controlIndex = new Set();

    return Promise.all([
        // recursivly search all nodes chronologically following start node,
        // with their parent control/wholePart nodes
        recursiveSearch([uri], true, nodesByUri, links, controlIndex),
        // recursivly search all nodes chronologically preceding start node,
        // with their parent control/wholePart nodes
        recursiveSearch([uri], false, nodesByUri, links, controlIndex),
        // go down one level of control/wholePart nodes
        searchDirectChild(uri, nodesByUri, links, controlIndex),
        // search ric:affect events
        searchEvents(uri),
    ]).then(([_, __, ___, events]) => {
        // remove nodes or links without begin date
        function filter(list, objType) {
            const result = [],
                discarded = [];
            for (const obj of list) {
                if (obj.begin === null) {
                    console.warn('skipping', objType, obj, ' with no begin date');
                    discarded.push(obj);
                } else {
                    result.push(obj);
                }
            }
            return [result, discarded];
        }
        const [nodes, discarded] = filter(Object.values(nodesByUri), 'node');
        let chronologicalLinks = filter(links.chronological, 'chronological link')[0],
            hierarchicalLinks = filter(links.hierarchical, 'hierarchical link')[0];

        // remove links referencing a discarded node
        const linksIndex = {};
        function filterLinkPredicate(key1, key2) {
            return function (link) {
                if (linksIndex[link.uri] === undefined) {
                    linksIndex[link.uri] = link;
                } else {
                    return false;
                }
                for (const node of discarded) {
                    if (link[key1] === node || link[key2] === node) {
                        return false;
                    }
                }
                return true;
            };
        }
        chronologicalLinks = chronologicalLinks.filter(
            filterLinkPredicate('preceding', 'following'),
        );
        hierarchicalLinks = hierarchicalLinks.filter(
            filterLinkPredicate('hierarchicalParent', 'hierarchicalChild'),
        );

        // fill nodes *Links attributes
        for (const link of chronologicalLinks) {
            link.preceding.followingLinks.push(link);
            link.following.precedingLinks.push(link);
        }
        for (const link of hierarchicalLinks) {
            link.hierarchicalParent.hierarchicalChildLinks.push(link);
            link.hierarchicalChild.hierarchicalParentLinks.push(link);
        }

        // we've a full and clean graph definition
        return { nodes, chronologicalLinks, hierarchicalLinks, events: packEvents(events) };
    });
}

function packEvents(events) {
    const packed = [];
    for (const evt of events) {
        if (packed.length) {
            const lastEvent = last(packed);
            if (lastEvent.year === evt.year) {
                lastEvent.description += `<br />${evt.description}`;
            } else {
                packed.push(evt);
            }
        } else {
            packed.push(evt);
        }
    }
    return packed;
}
// Function that fill `nodes` and `links` result structure by searching for
// following or preceding nodes (depending on the `forward` argument) and parent
// node. Function will consider URI in `todo` work list and each new URI
// discovered will be added to this work list.
//
// An additional `controlIndex` set has to be given to avoid potential duplicate
// queries looking for a node's hierarchical parents

function recursiveSearch(todo, forward, nodes, links, controlIndex) {
    const uri = todo.shift(),
        promises = [];
    if (forward) {
        promises.push(followingAgents(uri, nodes, links, todo));
    } else {
        promises.push(precedingAgents(uri, nodes, links, todo));
    }
    if (!controlIndex.has(uri)) {
        controlIndex.add(uri);
        promises.push(agentControlHeldBy(uri, nodes, links, todo));
        promises.push(agentPartOf(uri, nodes, links, todo));
    }
    return Promise.all(promises).then(_ => {
        if (todo.length === 0) {
            // we reached the end of the chain and the worklist is empty
            return [nodes, links];
        } else {
            return recursiveSearch(todo, forward, nodes, links, controlIndex);
        }
    });
}

// Function that fill `nodes` and `links` result structure by searching for
// direct child nodes, along with their following and preceding nodes
//
// An additional `controlIndex` set has to be given to avoid potential duplicate
// queries looking for a node's hierarchical parents

function searchDirectChild(uri, nodes, links, controlIndex) {
    const todo = [];

    return Promise.all([
        controlOnAgent(uri, nodes, links, todo),
        groupHasPart(uri, nodes, links, todo),
    ]).then(_ => {
        const promises = [];
        for (const childUri of todo) {
            controlIndex.add(childUri);
            promises.push(followingAgents(childUri, nodes, links));
            promises.push(precedingAgents(childUri, nodes, links));
        }
        return Promise.all(promises);
    });
}

/**
 * return the list of events that affect a given entity
 * @param {str} uri the entity we're trying to get related events on
 */
function searchEvents(uri) {
    return sparql(`
    PREFIX ric: <http://www.ica.org/standards/RiC/ontology#>

    SELECT ?event ?date ?label WHERE {
        ?event ric:date ?date ;
            rdfs:label ?label ;
            ric:affects <${uri}>.
    } ORDER BY ?date ?label
    `).then(bindings =>
        bindings.map(row => ({
            year: row.date.value,
            event: row.event.value,
            description: row.label.value,
        })),
    );
}
// function generating function to handle bindings of either of the 6 basic
// data retreival function, parametrized to generate the proper link
// definition.
function parametrizedProcessor(nodes, linksList, uri, uriKey, targetUriKey, relationType, todo) {
    return function processResults(bindings) {
        Array.prototype.map.call(bindings, binding => {
            const source = nodes[uri],
                targetUri = binding.target.value;
            if (nodes[targetUri] === undefined) {
                // todo being undefine means we only want relations between
                // existing nodes, so skip this one
                if (todo === undefined) {
                    return;
                }
                nodes[targetUri] = buildNode(
                    targetUri,
                    binding.targetLabel ? binding.targetLabel.value : '',
                    parseYear(binding.targetFromDate),
                    parseYear(binding.targetToDate, MAX_YEAR),
                );
                todo.push(targetUri);
            }

            const target = nodes[targetUri],
                link = {
                    uri: binding.rel.value,
                    [uriKey]: source,
                    [targetUriKey]: target,
                    begin: parseYear(binding.relBeginningDate),
                    end: parseYear(binding.relEndDate, MAX_YEAR),
                    relationType: relationType,
                };
            linksList.push(link);
        });
        return todo;
    };
}

function precedingAgents(uri, nodes, links, todo) {
    return sparql(temporalRelationQuery(uri, 'ric:agentFollows', 'ric:precedingAgent')).then(
        parametrizedProcessor(
            nodes,
            links['chronological'],
            uri,
            'following',
            'preceding',
            'chronological',
            todo,
        ),
    );
}

function followingAgents(uri, nodes, links, todo) {
    return sparql(temporalRelationQuery(uri, 'ric:agentPrecedes', 'ric:followingAgent')).then(
        parametrizedProcessor(
            nodes,
            links['chronological'],
            uri,
            'preceding',
            'following',
            'chronological',
            todo,
        ),
    );
}

function agentControlHeldBy(uri, nodes, links, todo) {
    return sparql(
        temporalRelationQuery(uri, 'ric:agentControlledBy', 'ric:agentControlHeldBy'),
    ).then(
        parametrizedProcessor(
            nodes,
            links['hierarchical'],
            uri,
            'hierarchicalChild',
            'hierarchicalParent',
            'control',
            todo,
        ),
    );
}

function controlOnAgent(uri, nodes, links, todo) {
    return sparql(
        temporalRelationQuery(uri, 'ric:hasAgentControlRelation', 'ric:controlOnAgent'),
    ).then(
        parametrizedProcessor(
            nodes,
            links['hierarchical'],
            uri,
            'hierarchicalParent',
            'hierarchicalChild',
            'control',
            todo,
        ),
    );
}

function agentPartOf(uri, nodes, links, todo) {
    return sparql(
        temporalRelationQuery(uri, 'ric:agentHasWholePartRelation', 'ric:agentPartOf'),
    ).then(
        parametrizedProcessor(
            nodes,
            links['hierarchical'],
            uri,
            'hierarchicalChild',
            'hierarchicalParent',
            'wholePart',
            todo,
        ),
    );
}

function groupHasPart(uri, nodes, links, todo) {
    return sparql(temporalRelationQuery(uri, 'ric:groupHasPart', 'ric:wholeHasAgentPart')).then(
        parametrizedProcessor(
            nodes,
            links['hierarchical'],
            uri,
            'hierarchicalParent',
            'hierarchicalChild',
            'wholePart',
            todo,
        ),
    );
}

function temporalRelationQuery(uri, resourceRelation, relationRelation) {
    return `
PREFIX ric: <http://www.ica.org/standards/RiC/ontology#>
SELECT ?target ?targetLabel ?targetFromDate ?targetToDate
       ?rel ?relBeginningDate ?relEndDate
WHERE {
    <${uri}> ${resourceRelation} ?rel.
    ?rel ${relationRelation} ?target.
    OPTIONAL {?rel ric:beginningDate ?relBeginningDate.}
    OPTIONAL {?rel ric:endDate ?relEndDate.}
    OPTIONAL {?target rdfs:label ?targetLabel.}
    OPTIONAL {?target ric:beginningDate ?targetBeginningDate.}
    OPTIONAL {?target ric:birthDate ?targetBirthDate.}
    BIND (COALESCE(?targetBirthDate, ?targetBeginningDate) as ?targetFromDate)
    OPTIONAL {?target ric:endDate ?targetEndDate.}
    OPTIONAL {?target ric:deathDate ?targetDeathDate.}
    BIND (COALESCE(?targetDeathDate, ?targetEndDate) as ?targetToDate)

}`;
}

function buildNode(uri, label, begin, end) {
    return {
        uri,
        label,
        begin,
        end: end || MAX_YEAR,
        precedingLinks: [],
        followingLinks: [],
        hierarchicalChildLinks: [],
        hierarchicalParentLinks: [],
    };
}

function takeFirst(attributes, prop1, prop2) {
    const values = attributes[prop1] || attributes[prop2];
    if (values) {
        return values[0];
    }
    return null;
}

function takeFirstInt(attributes, prop1, prop2) {
    const value = takeFirst(attributes, prop1, prop2);
    if (value) {
        return parseInt(value.split('-')[0]);
    }
    return null;
}
