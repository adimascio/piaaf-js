Mettre en place le démonstrateur
================================

## Récupération des données

```bash
git clone https://bitbucket.org/FlorenceClavaud/piaaf-project
```

Récupérer les données de référence de la DILA :

```bash
cd piaaf-project/rdf
wget http://echanges.dila.gouv.fr/OPENDATA/RefOrgaAdminEtat/FluxHistorique/2017-FluxCourant/dila_refOrga_admin_Etat_fr_latest.zip
unzip dila_refOrga_admin_Etat_fr_latest.zip
```


## Installation de virtuoso

Si vous possédez déjà un entrepôt SPARQL, vous pouvez sauter cette
étape. Sinon, vous pouvez utiliser le Dockerfile suivant :

```Dockerfile
FROM debian:jessie
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y install dpkg-dev build-essential wget bison gawk automake flex gperf libtool libssl-dev net-tools
RUN wget https://github.com/openlink/virtuoso-opensource/archive/stable/7.tar.gz
RUN tar xzf 7.tar.gz
RUN cd virtuoso-opensource-stable-7 && ./autogen.sh && ./configure --prefix=/usr/local/ && make -j4 && make install
EXPOSE 1111
EXPOSE 8890
CMD bash -c "cd /usr/local/var/lib/virtuoso/db && virtuoso-t -df"
```

et lancer la commande `docker build` à partir de celui-ci :

```bash

docker build -t virtuoso:base .
```

### Lancement du container virtuoso

Nous allons exposer le répertoire du projet `piaaf-project` que vous avez
cloné précédemment dans le volume `/inputs` :

```bash
docker run -p 8890:8890 -v /chemin/vers/piaaf-project:/inputs --restart=always virtuoso:base bash -c "cd /usr/local/var/lib/virtuoso/db && virtuoso-t -df"
```

Entrez dans le container démarré et autorisez le répertoire "/inputs" en l'ajoutant à "DirsAllowed" dans
`/usr/local/var/lib/virtuoso/db/virtuoso.ini`

### Import des données

Dans le container virtuoso :

```bash
root@dc79aa4043dca:~/# isql
ld_dir_all('/inputs/rdf', 'FRAN*.rdf', 'urn:piaaf:an');
ld_dir_all('/inputs/rdf', 'FRBNF*.rdf', 'urn:piaaf:bnf');
ld_dir_all('/inputs/rdf', 'FRSIAF*.rdf', 'urn:piaaf:siaf');
ld_dir('/inputs/rdf/', '*.rdf', 'urn:piaaf:data');
ld_dir_all('/inputs/rdf/vocabularies/', '*.rdf', 'urn:piaaf:data');
ld_dir_all('/inputs/rdf/ontology/', '*.rdf', 'urn:piaaf:owl');
rdf_loader_run();
SPARQL
PREFIX ric: <http://www.ica.org/standards/RiC/ontology#>
INSERT INTO GRAPH <urn:piaaf:data> { ?class rdfs:ancestorClass ric:Relation }
WHERE {
  ?class rdfs:subClassOf* ric:Relation.
};
SPARQL
  PREFIX owl: <http://www.w3.org/2002/07/owl#>
  INSERT INTO GRAPH <urn:piaaf:data> { ?object ?inverseRelation ?subject }
  WHERE {
    ?subject ?relation ?object.
    ?relation owl:inverseOf ?inverseRelation.
    FILTER(!isLiteral(?object) AND NOT EXISTS{?object ?inverseRelation ?subject})
  };
SPARQL
  PREFIX owl: <http://www.w3.org/2002/07/owl#>
  INSERT INTO GRAPH <urn:piaaf:data> { ?object ?relation ?subject }
  WHERE {
    ?subject ?inverseRelation ?object.
    ?relation owl:inverseOf ?inverseRelation.
    FILTER(!isLiteral(?object) AND NOT EXISTS{?object ?relation ?subject})
  };
SPARQL
  PREFIX ric: <http://www.ica.org/standards/RiC/ontology#>
  INSERT INTO GRAPH <urn:piaaf:data> { ?rs a ric:TopRecordSet }
  WHERE {
    ?rs a ric:RecordSet.
    FILTER(NOT EXISTS {?rs ric:memberOf ?parentRS}).
  };
DB.DBA.RDF_OBJ_FT_RULE_ADD(null, null, 'All');
DB.DBA.VT_INC_INDEX_DB_DBA_RDF_OBJ();
```

### Paramétrage de CORS


Connectez-vous sur http://localhost:8890/conductor, login dba / dba,
onglet "web application server" > "virtual domains" > "default web interface" >
"/sparl", mettre "*" dans "Cross-Origin Resource Sharing".


### Revenir à une base vide

```
root@dc79aa4043dca:~/# isql

RDF_GLOBAL_RESET();
DELETE FROM DB.DBA.load_list;
SPARQL
  SELECT  DISTINCT ?g
  FROM  <http://localhost:8894/>
  {
    GRAPH  ?g
      { ?s  a  ?t }
  } ;
```


### Effacer les triplets inférés

```
root@dc79aa4043dca:~/# isql

SPARQL WITH <urn:piaaf:data>
DELETE {?resource rdfs:ancestorClass ?class}
WHERE {?resource rdfs:ancestorClass ?class};

SPARQL WITH <urn:piaaf:data>
DELETE {?resource owl:sameAs ?class}
WHERE {?resource owl:sameAs ?class};

SPARQL WITH <urn:piaaf:data>
DELETE {?resource owl:differentFrom ?class}
WHERE {?resource owl:differentFrom ?class};

```

## Installation de piaaf-js

```bash
npm install
npm start
```

Par défaut, `piaaf-js` s'attend à trouver le *SPARQL endpoint* à l'adresse
`http://localhost:8890/sparql`. Si vous souhaitez utiliser une autre adresse,
vous pouvez positionner la variable d'environnement `SPARQL_ENDPOINT_URL` avant
de lancer `npm start`.

Par exemple :

```bash
export SPARQL_ENDPOINT_URL=http://piaaf.demo.logilab.fr/virtuoso/sparql
npm start
```
